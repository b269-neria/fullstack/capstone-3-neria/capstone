import { useContext} from 'react'
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar() {
   const {user} = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand as={Link} to="/">Capstone 3</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
          {user.isAdmin && <Nav.Link as={Link} to="/admin">Admin</Nav.Link>}
          { (user.id === null) ?
          <>
          <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
          <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
          </>
          :
          <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
          }

        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};
