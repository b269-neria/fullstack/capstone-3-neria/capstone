import { Button, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../App.css';

export default function ProductCard({product}) {
  const { productName, description, price, _id } = product;

  return (
    <Col xs={12} sm={6} md={4} lg={3} className="p-0 m-3">
      <Card className="cardHighlight productCardCss p-0 h-100 shadow">
        <Card.Body className="d-flex flex-column justify-content-between">
          <Card.Title><h4>{productName}</h4></Card.Title>
          <Card.Subtitle><h5>Description</h5></Card.Subtitle>
          <Card.Text className="productCardDescription">{description}</Card.Text>
          <Card.Subtitle><h5>Price</h5></Card.Subtitle>
          <Card.Text>₱ {price}</Card.Text>
          <Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>
        </Card.Body>
      </Card>
    </Col>
  );
};