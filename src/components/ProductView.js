import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import {useParams, Link, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import "../App.css"

export default function CourseView() {

	const {productId} = useParams();
	const navigate = useNavigate()
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const {user} = useContext(UserContext);

	const createOrder = (productId, quantity) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/new`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId:productId,
				quantity:quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Order Created!",
					icon: "success",
					text: "Order was successfully created with these details."
				})

				navigate("/products")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}

		})
	};


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setName(data.productName);
				setDescription(data.description);
				setPrice(data.price);
			})
	}, [productId])

	return (

		<Container className = "p-5">
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card className="shadow">
					      <Card.Body className="text-center">
					        <Card.Title><h3>{name}</h3></Card.Title>
					        <Card.Subtitle><h5>Description:</h5></Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle><h5>Price:</h5></Card.Subtitle>
					        <Card.Text>₱ {price}</Card.Text> 
					        <Form>
					          <Form.Group>
					            <Form.Label>Quantity:</Form.Label>
					            <Form.Control 
					            type="number" 
					            value={quantity} 
					            onChange={e => setQuantity(e.target.value)}
					            required
					            />
					          </Form.Group>
					        </Form>
					        {	
					        	(user.isAdmin === true) ?
					        		<Button className="btn btn-danger m-3" as={Link} to="/logout"  >An admin cannot create an order. Click here to logout first.</Button>
					        	:
					        	(user.id !== null) ?
					        		<Button className="m-3"pvariant="primary" onClick={() => createOrder(productId, quantity)} >Create Order</Button> 
					        	:
					        	<Button className="btn btn-danger m-3" as={Link} to="/login"  >Log in to Create an order</Button>
							}

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}
