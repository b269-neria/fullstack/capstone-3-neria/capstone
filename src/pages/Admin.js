import { Button, Modal, Form, Container, Table  } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useContext, useState } from 'react';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Admin() {
  const [showModal, setShowModal] = useState(false);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [productName, setProductName] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [productPrice, setProductPrice] = useState('');
  const {user} = useContext(UserContext);
  const handleShowModal = () => setShowModal(true);
  const handleCloseModal = () => setShowModal(false);
  const handleShowUpdateModal = () => setShowUpdateModal(true);
  const handleCloseUpdateModal = () => setShowUpdateModal(false);
  const [showTable, setShowTable] = useState(false);
  const handleShowProducts = () =>  setShowTable(!showTable);
  const [products, setProducts] = useState([]);
  const userToken = localStorage.getItem('token');
  const [toBeUpdateName, setToBeUpdatedName] = useState('');
  const [toBeUpdateDescription, setToBeUpdatedDescription] = useState('');
  const [toBeUpdatePrice, setToBeUpdatedPrice] = useState('');
  const [idToUpdate, setIdToUpdate] = useState('');


  const handleCreateProduct = (e) => {
    e.preventDefault();
    
    fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
      method: 'POST',
      headers:{
        Authorization: `Bearer ${userToken}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        productName: productName,
        description: productDescription,
        price: productPrice
      })
    })
    .then(res => res.json())
    .then(data => {
      if (data === true){
        Swal.fire({
                    title: "Product successfully created!",
                    icon: "success"
                })
        handleRetrieveProducts(userToken);
      } else {
                Swal.fire({
                    title: "There was an unexpected error",
                    icon: "error"
                })
      }
    })




    setProductName('');
    setProductDescription('');
    setProductPrice('');
    handleCloseModal();
  };

  const handleRetrieveProducts = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      headers:{
        Authorization: `Bearer ${token}`
      }

    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(data)});
  };

   const handleUpdateProduct = (product) => {
    setToBeUpdatedName(product.productName);
    setToBeUpdatedDescription(product.description);
    setToBeUpdatedPrice(product.price);
    setIdToUpdate(product._id);
  }; 

    const updateProduct = (e) => {
    e.preventDefault();
    
    fetch(`${process.env.REACT_APP_API_URL}/products/${idToUpdate}/update`, {
      method: 'PUT',
      headers:{
        Authorization: `Bearer ${userToken}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        productName: toBeUpdateName,
        description: toBeUpdateDescription,
        price: toBeUpdatePrice
      })
    })
    .then(res => res.json())
    .then(data => {
      if (data === true){
        Swal.fire({
                    title: "Product successfully updated!",
                    icon: "success"
                })
        handleRetrieveProducts(userToken);
      } else {
                Swal.fire({
                    title: "There was an unexpected error",
                    icon: "error"
                })
      }
    })

    setToBeUpdatedName('');
    setToBeUpdatedDescription('');
    setToBeUpdatedPrice('');
    setIdToUpdate('');
    handleCloseUpdateModal();      
  };


  const handleDisableProduct = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
      method: 'PATCH',
      headers:{
        Authorization: `Bearer ${userToken}`
      }
    })
    .then((res) => res.json())
    .then((data) => {
        handleRetrieveProducts(userToken);
  })
}


  const handleEnableProduct = (id) => {
  fetch(`${process.env.REACT_APP_API_URL}/products/${id}/activate`, {
      method: 'PATCH',
      headers:{
        Authorization: `Bearer ${userToken}`
      }
    })
    .then((res) => res.json())
    .then((data) => {
        handleRetrieveProducts(userToken);
  })
  };




   return (
    (user.isAdmin !== true) ?
    <Navigate to="/"/>
    :
    <Container className="d-flex flex-column align-items-center">
      <h2 className="mt-5">Welcome Admin, What would you like to do today?</h2>
        <div className="mt-5">
          <Button className="mx-3" onClick={handleShowModal}>
            Create a product
          </Button>
            <Button className="mx-3" variant="success"  onClick={() => 
            { handleRetrieveProducts(userToken);
              handleShowProducts();
            }}>Retrieve all products</Button>
        </div>
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Create a new product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleCreateProduct}>
            <Form.Group>
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product name"
                value={productName}
                onChange={e => setProductName(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter description"
                value={productDescription}
                onChange={e => setProductDescription(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                step="0.01"
                placeholder="Enter price"
                value={productPrice}
                onChange={e => setProductPrice(e.target.value)}
                required
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Create Product
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
       {showTable && (
        <Table variant="striped bordered hover">
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Price</th>
              <th>Availability</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {products.map((product) => (
                  <tr key={product._id}>
                    <td>{product.productName}</td>
                    <td>{product.description}</td>
                    <td>₱{product.price}</td>
                    <td>{product.isActive ? "Active" : "Inactive"}</td>
                    <td>
                      <Button
                        variant="secondary"
                        onClick={() => {
                          handleUpdateProduct(product);
                          handleShowUpdateModal();
                        }
                          
                      }
                      >
                        Update
                      </Button>{" "}
                      <Button
                        variant={product.isActive ? "danger" : "success"}
                        onClick={() =>
                          product.isActive
                            ? handleDisableProduct(product._id)
                            : handleEnableProduct(product._id)
                        }
                      >
                        {product.isActive ? "Disable" : "Enable"}
                      </Button>
                    </td>
                  </tr>
                ))}
          </tbody>
        </Table>
      )}
      <Modal show={showUpdateModal} onHide={handleCloseUpdateModal}>
        <Modal.Header closeButton>
          <Modal.Title>Update</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={updateProduct}>
            <Form.Group>
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product name"
                value={toBeUpdateName}
                onChange={e => setToBeUpdatedName(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter description"
                value={toBeUpdateDescription}
                onChange={e => setToBeUpdatedDescription(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                step="0.01"
                placeholder="Enter price"
                value={toBeUpdatePrice}
                onChange={e => setToBeUpdatedPrice(e.target.value)}
                required
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Update Product
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </Container>
    
);
}
