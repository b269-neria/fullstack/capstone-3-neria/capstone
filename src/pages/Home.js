import Banner from '../components/Banner';
import '../App.css';

export default function Home() {

	const data = {
		title: "Welcome to the Capstone 3 Market",
		content: "Here we'll be able to see the capabilities of an e-commerce app using the MERN stack",
		destination: "/products",
		label: "View the market"
	}


	return (
		<>
        <div className="home-container">
            <Banner data={data} />
        </div>

		</>
	)
}

