import {useState, useEffect} from 'react';
import ProductCard from '../components/ProductCard';

export default function Products() {
		const [products, setProducts] = useState([]);

useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/allActive`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} product={product} />
				)
			}))
		})
	}, [])


	return (
			<>
			<h2 class="text-center my-3">Welcome to the Market</h2>
			<h4 class="text-center my-2">These are our products</h4>
			<div className="d-flex flex-wrap justify-content-center">
				{products}
			</div>	
			</>
		)
}