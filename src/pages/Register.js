import React from 'react';
import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import '../App.css';
import Swal from 'sweetalert2';
import {Navigate, NavLink, useNavigate} from 'react-router-dom';

export default function Register() {

  const {user} = useContext(UserContext);
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [password2, setPassword2] = useState('')
  const [isActive, setIsActive] = useState(false)
  const navigate = useNavigate();

  function handleSubmit(e) {
    e.preventDefault();

    // Check if Email exists

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
            })
        })
        .then(res => res.json())
        .then(data => {
            // We will receive either a token or an error response.
            console.log(data);
            if (data === false){
            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
          console.log (data)
            
            // Clear input fields
            setEmail("");
            setPassword("");
            setPassword2("");

           Swal.fire({
                    title: "Registration Successful!",
                    icon: "success",
                    text: "Try logging in now"
                })
           navigate("/login")

        })

            } else {
                Swal.fire({
                    title: "Duplicate Email Found",
                    icon: "error",
                    text: "Please provide a different email"
                })
            }
        
    })

  };

  useEffect(() => {
    if((email !== "" && password !== "" && password2 !== "")&&(password === password2)){
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  },[email,password,password2]);


  return (
(user.id !== null) ?
<Navigate to="/"/>
:
<div className="input-page">
      <div className="input-form">
        <Form onSubmit={handleSubmit} className="text-center">
          <h3>Register</h3>
          <Form.Group controlId="formBasicEmail" className="mt-3">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
            type="email" 
            placeholder="Enter email" 
            value={email}
            onChange={e => setEmail(e.target.value)}
            required />
          </Form.Group>

          <Form.Group controlId="formBasicPassword" className="mt-3">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" 
            placeholder="Password" 
            value={password}
            onChange={e => setPassword(e.target.value)}
            required/>
          </Form.Group> 

          <Form.Group controlId="formConfirmPassword" className="mt-3">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control type="password" 
            placeholder="Verify Password" 
            value={password2}
            onChange={e => setPassword2(e.target.value)}
            required/>
          </Form.Group>

          { isActive ?
          <Button variant="primary" type="submit" className="mt-3">
            Register
          </Button>
          :
          <Button variant="danger" type="submit" className="mt-3" disabled>
            Enter credentials first
          </Button>
          }

          <div className="mt-3">
            Already have an account?{' '}
            <NavLink as={Button} variant="link" to="/login">
               Login here!
            </NavLink>
          </div>
        </Form>
      </div>
    </div>
  );
};
